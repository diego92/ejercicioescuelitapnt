package clases;

public class Bebida extends Producto{

	private double _litros;
	
	public Bebida(String nombre, int precio, double litros) {
		super(nombre, precio);
		_litros = litros;
	}

	public double getLitros() {
		return this._litros;
	}

	public void setLitros(double litros) {
		this._litros = litros;
	}

	@Override
	public String toString() {
		return "Nombre: "+getNombre()+" /// Litros: "+_litros+" /// Precio: $"+getPrecio();
	}
	
	
}
