package clases;

public class Fruta extends Producto{

	private String _unidadDeVenta;
	
	public Fruta(String nombre, int precio, String unidadDeVenta) {
		super(nombre, precio);
		_unidadDeVenta = unidadDeVenta;
	}

	public String get_unidadDeVenta() {
		return _unidadDeVenta;
	}

	public void set_unidadDeVenta(String _unidadDeVenta) {
		this._unidadDeVenta = _unidadDeVenta;
	}

	@Override
	public String toString() {
		return "Nombre: "+getNombre()+" /// Precio: $"+getPrecio()+" /// Unidad de venta: "+_unidadDeVenta;
	}
}
