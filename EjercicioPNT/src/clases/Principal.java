package clases;

import java.util.ArrayList;
import java.util.Collections;

public class Principal {
	
	private static ArrayList<Producto> productos;
	
	public static void main(String[] args) {
		
		inicializarLista();
		cargarProductos();
		imprimirProductos();
		
		System.out.println("=============================");
		
		ordenarProductos();
		
		imprimirInformacion();
	}

	private static void inicializarLista() {
		
		productos = new ArrayList<Producto>();
	}

	private static void cargarProductos() {
		
		productos.add(new Bebida("Coca-Cola Zero", 20, 1.5));
		productos.add(new Bebida("Coca-Cola", 18, 1.5));
		productos.add(new Shampoo("Shampoo Sedal", 19, 500));
		productos.add(new Fruta("Frutillas", 64, "kilo"));
	}
	
	private static void imprimirProductos() {
		
		for(Producto p : productos)
			System.out.println(p.toString());
	}
	
	private static void ordenarProductos() {
		
		Collections.sort(productos);
	}
	
	private static void imprimirInformacion() {
		System.out.println("Producto m�s caro: "+productos.get(productos.size()-1).getNombre());
		System.out.println("Producto m�s barato: "+productos.get(0).getNombre());
	}
}