package clases;

public class Shampoo extends Producto{
	
	private int _contenido;
	
	public Shampoo(String nombre, int precio, int contenido) {
		super(nombre, precio);
		_contenido = contenido;
	}

	public int get_contenido() {
		return _contenido;
	}



	public void set_contenido(int _contenido) {
		this._contenido = _contenido;
	}


	@Override
	public String toString() {
		return "Nombre: "+getNombre()+" /// Contenido: "+_contenido+"ml /// Precio: $"+getPrecio();
	}
}
